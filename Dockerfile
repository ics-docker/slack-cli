FROM python:3.7-slim

LABEL maintainer="benjamin.bertrand@esss.se"

RUN groupadd -r -g 1000 csi \
  && useradd --no-log-init -r -m -g csi -u 1000 csi

RUN python -m venv /venv \
  && . /venv/bin/activate \
  && pip install --no-cache-dir slack-cli==2.2.5

ENV PATH /venv/bin:$PATH

USER csi
