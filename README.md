# slack-cli

Docker image to send slack notifications.
It inlcudes the [slack-cli](https://github.com/regisb/slack-cli) command line tool.

## Usage

This image is meant to be used in a `.gitlab-ci.yml` job. To send a message to the bots channel:

```
slack-notification:
  image: registry.esss.lu.se/ics-docker/slack-cli
  script:
    - slack-cli -t $SLACK_API_TOKEN -d bots "Hello World!"
```

Note that the `slack-cli-bot` app must be added to the channel to allow the bot to write to it.
